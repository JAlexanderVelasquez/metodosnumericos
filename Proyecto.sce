//Alexander Velasquez 
//Fecha última modificación 5/06/18
//Este programa contiene los 4 metodos, metodo de biseccion, polinomio interpolante de lagrange, minimos cuadrados y regla compuesta del trapecio. 

printf("Este programa contiene los 4 metodos, metodo de biseccion(1), polinomio interpolante de lagrange(2), \nminimos cuadrados(3) y regla compuesta del trapecio(4).\n Para selecionar un método digite un número del 1 al 4 si desea salir seleccione 0\n")
op=input("digite su opcion: ","s");
    select op,
  case "1" then 
        clc()//limpia la consola
        printf("\n Bienvenido al programa que usa el Método de biseccion, \ndespués de cada instrucción pulse la tecla <enter>: ")
        s=input("Ingrese la funcion a evaluar: ","s")//ingresa la funcion como string
        deff('[y]=problema(x)', 'y='+s);//convierte ese string a una funcion
        //las siguientes 3 variables son para definir en que intervalo y con que exactitud se va graficar la funcion
        xl= input("\n Ingrese el limite inferior del intervalo para la grafica: ");
        xu= input("\n Ingrese el limite superior del intervalo para la grafica: ");
        xs= input("\n Ingrese cada cuanto quiere que grafique, por ejemplo cada 0.1 puntos entre ese intervalo: ");
        //se creea el intervalo con las especificaciones del usuario
        x=xl:xs:xu
        //grafica la funcion en ese intervalo
        plot2d(x,problema(x));
        xgrid(5);
        //esta variable nos indica en que iteracion estamos
        i=1
        //los limites del intervalo [a,b]
        a= input("\n Ingrese el limite inferior del intervalo: ");
        b= input("\n Ingrese el limite superior del intervalo: ");
        //La tolerancia al error con la que se trabajará
        tao= input("\n Ingrese el error deseado o tao: ");
        //En caso de no querer una ejecución demasiado extensa por el tao definido se puede limitar la cantidad de iteraciones con las que trabajar, en caso de no definir un limite el limite por defecto es 100
        it= input("\n ¿Desea tener un limite de iteraciones? en caso afirmativo ingrese 1, de lo contrario 0: ");
        if(it==1)
            maxN= input("\n Ingrese un maximo de iteraciones: ");
        end
        if(it==0)
            maxN=100;
        end
        //se evalua la función en el punto a
        fa=feval(a, problema);
        //se evalua la función en el punto b
        fb=feval(b, problema);
        //se calcula la raiz para esa iteracion
        raiz=(a+b)/2;
        //se define un error actual base dado que la primera iteracion compara el error actual con el tao
        eact=100;
        //se evalua la funcion en la raiz calculada anteriormente para luego poder graficar los puntos que aproxima el metodo de biseccion
        fraiz=feval(raiz,problema)
        //determina si hay cambio de signo entre las imagenes de la funcion en los puntos a y b lo que nos permite saber si hay algún c tal que f(c)=0 en caso contrario no hay una solución o raíz en ese intervalo
        if(fa*fb < 0)
            //se realiza el método de biseccion mientras que el error actual no sea menor que el tao pedido por el usuario
            while(eact >= tao )
                //muestra en consola la cantidad de iteraciones, el a,el b, la raíz, la funcion evaluada en esa raíz y el error para esa ejecución
                printf("\n N=%1.0f | A=%1.5f|B=%1.5f| Raiz=%1.5f| F(raiz)=%1.5f|Error=%1.5f",i,a,b,raiz,fraiz,eact);
                //si la iteraccion actual es igual al limite definido deja de iterar
                if(i==maxN)
                    break
                end
                fraiz=feval(raiz, problema)
                //se define caso 1 que es el resultado de multiplicar la funcion evaluada en a por la funcion evaluada en la raiz, el caso 2 es lo mismo solo que se multiplica la funcion evaluada en b por la funcion evaluada en la raiz para asi poder determinar en cual intervalo hay cambio de signo lo que implica que en ese intervalo hay solucion
                caso1=fa*fraiz;
                caso2=fb*fraiz;
                //en los siguientes dos if lo que se hace es asignar el valor de la raiz y su imagen al punto a y su imagen fa o al punto b y su imagen fb dependiendo del caso, en ambos casos tambien se asigna a la raiz anterior el valor que tenia la raiz para ese punto y se calcula una nueva raiz con el nuevo intervalo, ractual es la raiz actual que se le asigna el valor de la raiz calculada
                if(caso1<0)
                    b=raiz;
                    fb=fraiz;
                    ranterior=raiz;
                    raiz=(a+b)/2;
                    ractual=raiz;
                end
                if(caso2<0)
                    a=raiz;
                    fa=fraiz;
                    ranterior=raiz;
                    raiz=(a+b)/2;
                    ractual=raiz;
                end
                //aumenta la cantidad de iteraciones en uno
                i=i+1
                //se calcula el error actual
                eact=abs((ractual-ranterior)/ractual)*100;
                //se grafica el punto que aproxima a la funcion para esa iteracion
                plot(raiz,fraiz,".");
            end
        //se imprime el resultado para la ultima ejecución que consta de la iteracion, el error actual y la raíz actual
        printf("\n Al finalizar las iteraciones se obtiene: \n n= %d | Error= %f | Raiz=%1.5f",i,eact,ractual);
        //se le informa al usuario que ya puede ver las aproximaciones en la misma grafica de la funcion
        printf("\n Mire en la grafíca las aproximaciones de los puntos.")
        end
        //se imprime que no hay raiz en el intervalo en caso de que no sea un intervalo valido
        if(fa*fb > 0)
            printf("No existe una raiz en ese intervalo");
        end,
        
  case "2" then      
        function [Pn,polinomio]=PILag(M,x)
            [m n] = size(M);
            Pn= zeros(1,length(x));
        s=input("Ingrese la funcion original, en la grafica será la funcion de color rojo: ","s")//ingresa la funcion como string
        deff('[y]=problema(x)', 'y='+s);
        plot2d(x,problema(x),5);
            polinomio="Polinomio= ";
            printf(polinomio);
            //termino=""
            //se crea una matriz de tamaño 1xn que almacenará los Ln,k
            producto = ones(1,length(x))
            for i= 1:n
                //se agrega el yi al string termino para poder construir el polinomio
                termino= '' + mtlb_num2str(M(2,i));
                for j =1:n
                    if i<>j 
                        producto = producto .* (x-M(1,j))/(M(1,i)-M(1,j));
                        //se agrega agrega el Ln,j
                        termino= termino + '*(x-' + mtlb_num2str(M(1,j))+')/('+ mtlb_num2str(M(1,i))+'-'+mtlb_num2str(M(1,j))+')';
                    end
                end
                //se calcucula el Pn para la iteracion i
                Pn = Pn + M(2,i)*producto
                //Con los 2 if siguientes se agrega el string a polinomio para determinar el polinomio calculado y se imprime pero con una diferencia, si es la última iteracion no se le agrega el '  + '  que indicaría la suma con el siguiente Ln,k
                if(i<>n)
                    polinomio= ' '+ termino + '  + ' ;
                    printf(polinomio);
                end
                if(i==n)
                   polinomio=" " + termino;
                   printf(polinomio); 
                end
            end
            for i=1:n
                plot(M(1,i),M(2,i),".");
            end
            [a,aa]=resume(Pn,polinomio)
        endfunction
        //limpia la consola
        clc;
        //Se imprimen en consola las instrucciones para usar este programa paso a paso
        printf("\n Bienvenido a la función que calcula el Polinomio interpolante de Lagrange, donde usted podrá visualizar la función\n original(color rojo) los puntos ingresados(color azul) y si desea el polinomio interpolante(color azul), para hacer\n uso de este programa siga las instrucciones a continuación, después de cada instrucción pulse la tecla <enter>: ")
        printf("\n *Ingrese un vector de la forma x=-3:0.01:3; que delimitará el rango y la precisión de la grafica despues: ")
        printf("\n *Ingrese una matriz de la forma A=[0 1 2 3; -1 -2 3 2]; donde la primer fila de numeros define los x´s y la segunda\n los y´s que serán los puntos con los cuales se hallará el polinomio: ")
        printf("\n *Ahora escriba y=PILag(A,x); para visualizar el polinomio: ")
        printf("\n *Si desea ver la grafica del polinomio, de color azul, escriba plot2d(x,y,2) : "),

      case "3" then 
          printf("\n Bienvenido a la función que calcula una funcion que aproxima una serie de puntos,mediante los minimos cuadrados\n los puntos ingresados(color rojo) y la funcion (color azul) para hacer\n uso de este programa siga las instrucciones a continuación, después de cada instrucción pulse la tecla <enter>: ")
        printf("\n *Ingrese un vector de la forma x=[1 2 3 4]; que delimitará los x´s de los nodos a evaluar: ")
        printf("\n *Ingrese otro vector de la forma de la forma y=[-1 -2 3 2]; que define los y´s de los nodos: ")
        printf("\n *Ingrese un numero de la forma n=10 que indica el grado del polinomio resultante: ")
        printf("\n *Ingrese el limite de la gráfica de la forma XX=0:0.1:5;  : ")
        printf("\n *Ahora escriba z=MinimosCuadrados(x,y,n) para visualizar el polinomio: ")
                  function salida =MinimosCuadrados(x,y,n)
            salida="Fin del programa.";
            printf("Matriz: \n")
        //se hace un doble for para crear la matriz y compltarla es de tamaño nxn
            for i=1:n+1
                for j=1:n+1
                    M(i,j)=sum(x.^(i+j-2))//los valores de xi en cada posicion de la matriz
                    printf(mtlb_num2str(M(i,j)) + "  ")
                end
                printf("\n")
            end
            Inversa=inv(M);
            printf("\n Matriz inversa: \n")
            for i=1:n+1
                for j=1:n+1
                    printf(mtlb_num2str(Inversa(i,j)) + "  ")
                end
                printf("\n")
            end
            //se crea y completa el vector b de la forma y*x^j; para j=0,1,2,...,n
            printf("\nVector b: \n")
            for i=1:n+1
                b(i)=sum(y.*x.^(i-1))
                printf(mtlb_num2str(b(i)) + "  ")
            end
            //a será el resultado de multiplicar la inversa de la matriz M con el vector b
        
            a=Inversa*b;
            //los puntos y de la función
            YY = 0
            printf("\n \n Vector a: \n")
            for i=1:n+1
                YY=YY+a(i)*XX.^(i-1);
                printf(mtlb_num2str(a(i)) + "  ")
            end
            plot(XX,YY)
            plot(x,y,'r.');xgrid
            printf("\n\nMire la aproximación de los puntos dados, con el polinomio de grado "+mtlb_num2str(n) +" en la gráfica")
            printf("\n\n")
            return salida;
        endfunction ,

  case "4" then 
        clc;
        funcprot(0)
        //Se imprimen en consola las instrucciones para usar este programa paso a paso
        printf("\n Bienvenido a la función que calcula la integral mediante la regla compuesta del trapecio para hacer uso de \neste programa siga las instrucciones a continuación, después de cada instrucción pulse la tecla <enter>: ")
        s=input("Ingrese la funcion a integrar: ","s")//ingresa la funcion como string
        deff('[y]=funcion(x)', 'y='+s);
        printf("\n *Ingrese un valor para a que es el limete inferior de integracion de la forma\n a=0; : ")
        printf("\n *Ahora ingrese un valor para b que es el limete superior de integracion de la forma\n b=1; : ")
        printf("\n *Ahora ingrese un valor para n que es la cantidad de intervalos de la forma\n n=500; : ")
        
        printf("\n *Ahora escriba integral=ReglaTrapecio(a,b,n) para calcular la integral: ")
        function [area]=ReglaTrapecio(a,b,n)
            x=a:0.01:b;
            plot2d(x,funcion(x),2);
            area=0; //se inicia la variable area con un valor inicial de 0
            h=(b-a)/n;//se calcula el h
        
            area=funcion(a);//primer termino 
            
            xj=a;//se calcula el xj que inicialmente es a para j=0,  xj=a+0*h
            printf("x0 = "+mtlb_num2str(xj)+ " f(x0) = "+ mtlb_num2str(funcion(xj)) + "\n")
            for i=1:n
                xj=xj+h;//xj=a+j*h
                area=area+2*funcion(xj);//cada uno de los terminos de la sumatoria
                printf("x"+ mtlb_num2str(i) +" = "+mtlb_num2str(xj)+ " f(x" +mtlb_num2str(i)+") = "+ mtlb_num2str(funcion(xj)) + "\n")
            end
            area=area+funcion(xj+h);//ultimo termino de la serie f(b)
            area=area*(h/2);//se multiplica por el factor común que es h/2
            x=linspace(a,b,n)
            bar(x,funcion(x))
        endfunction,

  else 
      
  end
