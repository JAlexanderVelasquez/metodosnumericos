# MetodosNumericos

En este proyecto se usa Scilab para implementar 4 métodos numéricos, método de bisección, polinomio interpolante de Lagrange, mínimos cuadrados y regla compuesta del trapecio.

# Guia de uso

Para usar este proyecto se debe tener instalado Scilab, una vez se descargue el codigo se ejecuta y aparece un menu en el cual se explica el proceso que se debe seguir en cada uno de los métodos además de la información que se debe suministrar.